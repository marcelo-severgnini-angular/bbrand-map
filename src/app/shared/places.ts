export interface Places {
  $key: string;
  city: string;
  description: string;
  extra: string;
  id: Number,
  img: string,
  latitude: string,
  longitude: string,
  qt: string
}
