import {Vector3} from "three";

export class TweenModel {
  tweenID: number;
  radiusToSpace: number;
  radiusToTarget: number;
  landPosition: Vector3;
  spacePosition: Vector3;
  speedToSpace: number;
  speedToTarget: number;

  constructor(tweenID, radiusToSpace, radiusToTarget, speedToSpace, speedToTarget) {
    this.tweenID = tweenID;
    this.radiusToSpace = radiusToSpace;
    this.radiusToTarget = radiusToTarget;
    this.landPosition = new Vector3();
    this.spacePosition = new Vector3();
    this.speedToSpace = speedToSpace;
    this.speedToTarget = speedToTarget;
  }
}
