import { Object3D, Mesh, Scene, WebGLRenderer, PerspectiveCamera } from 'three';
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls";
import TWEEN from "@tweenjs/tween.js";

export class Globe {
    earthMesh: Mesh;
    cloudsMesh: Mesh;
    earth: Object3D; // ok
    scene: Scene; // ok
    renderer: WebGLRenderer; // ok
    camera: PerspectiveCamera;
    control: OrbitControls;
    width: number;
    height: number;
    private tweens: Array<TWEEN.Tween>;

    constructor(width, height) {
        this.width = width;
        this.height = height;
        this.scene = new Scene();
        this.renderer = new WebGLRenderer({ antialias: false });
        this.earth = new Object3D();
        this.renderer.setClearColor(0xf0f0f0);
        this.renderer.setSize(this.width, this.height);
        this.tweens = [];
    }

    addTween(tween: TWEEN.tween) {
      this.tweens.push(tween);
    }

    getTween(index: number) {
      return this.tweens[index];
    }
}
