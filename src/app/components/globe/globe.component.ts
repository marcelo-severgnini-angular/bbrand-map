import {Component, OnInit, ElementRef, AfterViewInit, ViewChild, OnDestroy, HostListener} from '@angular/core';
import { SceneBuilderService } from 'src/app/service/app.scene.builder.service';
import { Globe } from 'src/app/shared/globe';
import OrbitControls from 'three-orbitcontrols';
import {FirebaseService} from "../../service/firebase.service";
import {ResizeService} from "../../service/resize.service";
import {Subscription} from "rxjs";
import {TweenChainService} from "../../service/tween.chain.service";
import TWEEN from "@tweenjs/tween.js";
import {Object3D, Raycaster, Vector2} from "three";

@Component({
  selector: 'app-globe',
  templateUrl: './globe.component.html',
  styleUrls: ['./globe.component.css'],
  providers: [ResizeService],
})
export class GlobeComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('container', {static: false}) container: ElementRef;
  canvas: any;
  globe: Globe;
  private resizeSubscription: Subscription;
  mouse = new Vector2();
  raycaster = new Raycaster();
  userData: {};

  constructor(private sceneBuilder: SceneBuilderService, private firebaseService: FirebaseService, private resizeService: ResizeService, private tweenService: TweenChainService) {}

  ngAfterViewInit() {
    this.initialize();
    this.buildScene();
    this.buildLights();
    this.setCamera();
    this.setControls();

    let values = this.firebaseService.getPlaces();
    values.once("value", )
      .then((snapshot) => {
        this.firebaseService.placeList = snapshot.val();
      }).then(() => {
        this.firebaseService.placeList.forEach((place) => {
          const marker = this.sceneBuilder.setMarker(place);
          this.globe.earth.add(marker);
        });
      }).then(() => {
        this.tweenService.createTween(this.globe, this.firebaseService.placeList[0]);
        // this.globe.getTween(0).delay(500).start();
        this.animate();
    });
  }

  @HostListener('mousemove', ['$event'])
  onMousemove(event: MouseEvent) {
    event.preventDefault();
    // this.mouse.x = ( event.clientX / this.canvas.offsetWidth ) * 2 - 1;
    // this.mouse.y = - ( event.clientY / this.canvas.offsetHeight ) * 2 + 1;

    this.mouse.x = ((event.clientX ) / this.canvas.offsetWidth) * 2 - 1;
    this.mouse.y = -((event.clientY) / this.canvas.offsetHeight) * 2 + 1;

    this.globe.camera.lookAt( this.globe.scene.position );
    this.globe.camera.updateMatrixWorld();
  }

  @HostListener('click', ['$event'])
  onMouseClick(event: MouseEvent) {
    event.preventDefault();
    this.globe.control.enabled = false;
    this.raycaster.setFromCamera( this.mouse, this.globe.camera );

    const intersects = this.raycaster.intersectObjects(this.globe.scene.children[1].children, true);

    if (intersects.length > 0) {
      if(intersects[0].object instanceof Object3D){
        this.userData = JSON.stringify(intersects[0].object);
      }
    } else {
      this.userData = {empty: "true"};
    }
    this.globe.control.enabled = true;
  }

  initialize() {
    this.globe = new Globe(this.container.nativeElement.offsetWidth, this.container.nativeElement.offsetHeight);
  }
 
  buildScene() {
    this.globe.earthMesh = this.sceneBuilder.buildEarth();
    this.globe.cloudsMesh =  this.sceneBuilder.buildCloud(this.globe.width, this.globe.height);
    this.globe.scene.add(this.sceneBuilder.buildSkyBox());
    this.globe.earth.add(this.globe.earthMesh);
    this.globe.earth.add(this.globe.cloudsMesh);
    this.globe.scene.add(this.globe.earth);
    this.canvas = this.globe.renderer.domElement;
    this.container.nativeElement.appendChild(this.canvas);
  }

  buildLights() {
    this.globe.scene.add(this.sceneBuilder.buildLight({color: "white", x: 0, y: 200, z: 0}));
    this.globe.scene.add(this.sceneBuilder.buildLight({color: "white", x: 200, y: -200, z: 0}));
  }

  setCamera() {
    this.globe.camera = this.sceneBuilder.buildCamera(this.globe.width / this.globe.height);
    // console.log(`Canvas Width & Height -> ${this.globe.width} - ${this.globe.height} - Camera -> \n ${JSON.stringify(this.globe.camera)}`);
  }

  setControls() {
    this.globe.control = new OrbitControls(this.globe.camera);
    this.globe.control.minDistance = 65;
    this.globe.control.maxDistance = 230;
    this.globe.control.enabled = false;
  }

  animate(time?: number) {

    requestAnimationFrame(() => {
      this.animate();
    });

    this.updateCameraMatrix();

    if (this.globe.control.enabled) {
      this.globe.control.update();
    }
    this.globe.cloudsMesh.rotation.y += 0.0001;
    this.globe.renderer.clear();
    this.globe.renderer.render(this.globe.scene, this.globe.camera);
    // TWEEN.update(time);
  }

  updateCameraMatrix() {
    // if(this.resizeRendererToDisplaySize()){
    this.resizeRendererToDisplaySize();
    const canvas = this.globe.renderer.domElement;
    this.globe.camera.aspect = canvas.clientWidth / canvas.clientHeight;
    this.globe.camera.updateProjectionMatrix();
    // }
  }

  resizeRendererToDisplaySize() {
    const canvas = this.container.nativeElement;
    const width = canvas.clientWidth;
    const height = canvas.clientHeight;
    const needResize = canvas.offsetWidth !== width || canvas.offsetHeight !== height;
    // console.log(`${canvas.offsetWidth !== width || canvas.offsetHeight !== height}`);
    // if (needResize) {
      this.globe.renderer.setSize(width, height, true);
    // }
    return needResize;
  }

  ngOnInit() {
    // this.resizeSubscription = this.resizeService.onResize$
    //   .subscribe((size) => this.updateCameraMatrix());
  }

  ngOnDestroy() {
    if (this.resizeSubscription) {
      this.resizeSubscription.unsubscribe();
    }
  }
}
