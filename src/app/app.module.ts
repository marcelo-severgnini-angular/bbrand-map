import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { LoadingComponent } from './components/loading/loading.component';
import { HttpClientModule } from '@angular/common/http';
import {AppHttpClienteService} from "./service/app.httpClienteService";
import { SceneBuilderService } from './service/app.scene.builder.service';
import { GlobeComponent } from './components/globe/globe.component';
import { CountryComponent } from './components/country/country.component';
import {AngularFireModule} from "@angular/fire";
import {AngularFireDatabaseModule} from "@angular/fire/database";
import {environment} from "../environments/environment";
import {UtilService} from "./service/util.service";
import {TweenChainService} from "./service/tween.chain.service";
import {FirebaseService} from "./service/firebase.service";


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoadingComponent,
    GlobeComponent,
    CountryComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
  ],
  providers: [SceneBuilderService, AppHttpClienteService, AngularFireDatabaseModule, UtilService, TweenChainService, FirebaseService],
  bootstrap: [AppComponent]
})
export class AppModule { }
