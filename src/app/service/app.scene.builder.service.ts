import {
  TextureLoader,
  Texture,
  SphereGeometry,
  MeshPhongMaterial,
  Color,
  Mesh,
  LoadingManager,
  DoubleSide,
  MeshBasicMaterial,
  BackSide,
  DirectionalLight,
  PerspectiveCamera,
  CylinderGeometry, Vector3, Object3D, Euler
} from 'three';
import {environment} from "../../environments/environment";


export class SceneBuilderService {

  loadManager: LoadingManager;

  constructor() {
    let onLoaded = () => {
      // console.log("Start loading");
    };

    this.loadManager = new LoadingManager(onLoaded);

    this.loadManager.onProgress = (url, itemsLoaded, itemsTotal) => {
      // console.log(url);
      // console.log(itemsLoaded / itemsTotal * 100);
    };

    this.loadManager.onError = (url) => {
      // console.log("Error loading texture: " + url);
    };
  }
  buildEarth = () => {

        let mapLoader: Texture = new TextureLoader(this.loadManager).load(`${environment.basePath}/assets/textures/world-big.jpg`);
        // let bumpMapLoader = new TextureLoader(this.loadManager).load("/assets/textures/world-big.jpg");
        // let specularMapLoader = new TextureLoader(this.loadManager).load("/assets/textures/world-big.jpg");

        let geometry = new SphereGeometry(50, 32, 32);
        let material = new MeshPhongMaterial({
            map: mapLoader,
            // bumpMap: bumpMapLoader,
            bumpMap: mapLoader,
            bumpScale: 0.05,
            shininess: 100,
            // specularMap: specularMapLoader,
            specularMap: mapLoader,
            specular: new Color("grey")
        });
        return new Mesh(geometry, material);
    };


  buildCloud = (width, height) => {

      let canvasResult = document.createElement("canvas");
      canvasResult.width = width;
      canvasResult.height = height;
      let contextResult = canvasResult.getContext("2d");

      let imageMap = new Image();
      imageMap.addEventListener("load", () => {
          let canvasMap = document.createElement("canvas");
          canvasMap.width = imageMap.width;
          canvasMap.height = imageMap.height;
          let contextMap = canvasMap.getContext("2d");
          contextMap.drawImage(imageMap, 0, 0);
          let dataMap = contextMap.getImageData(0, 0, canvasMap.width, canvasMap.height);

          let imageTrans = new Image();
          imageTrans.addEventListener("load", () => {
            let canvasTrans = document.createElement("canvas");
            canvasTrans.width = imageTrans.width;
            canvasTrans.height = imageTrans.height;
            let contextTrans = canvasTrans.getContext("2d");
            contextTrans.drawImage(imageTrans, 0, 0);
            let dataTrans = contextTrans.getImageData(0, 0, canvasTrans.width, canvasTrans.height);
            let dataResult = contextMap.createImageData( canvasMap.width, canvasMap.height );
            for (let y = 0, offset = 0; y < imageMap.height; y++) {
              for (let x = 0; x < imageMap.width; x++, offset += 4) {
                dataResult.data[offset + 0] = dataMap.data[offset + 0];
                dataResult.data[offset + 1] = dataMap.data[offset + 1];
                dataResult.data[offset + 2] = dataMap.data[offset + 2];
                dataResult.data[offset + 3] = 255 - dataTrans.data[offset + 0];
              }
            }
            contextResult.putImageData(dataResult, 0, 0);
            material.map.needsUpdate = true;
          });
          imageTrans.src = `${environment.basePath}/assets/textures/earthcloudmaptrans.jpg`;
        }, false
      );

      imageMap.src = `${environment.basePath}/assets/textures/earthcloudmap.jpg`;

      let geometry = new SphereGeometry(50.5, 32, 32);

      let material = new MeshPhongMaterial({ map: new Texture(canvasResult), side: DoubleSide, transparent: true, opacity: 0.8 });

      return new Mesh(geometry, material);
    };

    buildSkyBox = () => {
        let texture = new TextureLoader(this.loadManager).load(`${environment.basePath}/assets/textures/galaxy_starfield.png`);
        let material = new MeshBasicMaterial({ map: texture, side: BackSide });
        let geometry = new SphereGeometry(1000, 32, 32);
        return new Mesh(geometry, material);
    };

    buildLight = (lightProps: {color: string, x: number, y: number, z: number}) => {
        let light = new DirectionalLight(lightProps.color);
        light.position.set(lightProps.x, lightProps.y, lightProps.z);
        return light;
    };

    buildCamera = (aspect: number) => {
        let fov = 45;
        let near = 1;
        let far = 100000;
        let camera = new PerspectiveCamera(fov, aspect, near, far);
        camera.position.set(0, 0, 200);
        return camera;
    };

    setMarker = (city) : Object3D => {
      let rad = Math.PI / 180;
      let marker = this.createMarker(city);
      marker.name = city.city.replace(/ /g, "-").toLowerCase();
      marker.quaternion.setFromEuler(new Euler(0, city.longitude * rad, city.latitude * rad, "YZX"));
      return marker;
    };

    private createMarker = (city) => {
      let pointer = new Mesh(
        new CylinderGeometry(0.5, 3, 10),
        new MeshPhongMaterial({ color: 0xff5c01 })
      );
 ;
      pointer.position.set(50, 0, 0);
      pointer.quaternion.setFromUnitVectors(
        new Vector3(0, 1, 0),
        new Vector3(1, 0, 0)
      );

      let marker = new Object3D();
      marker.userData = city;
      marker.add(pointer);
      return marker;
    }

}
