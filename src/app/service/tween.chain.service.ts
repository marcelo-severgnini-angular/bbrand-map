import {Globe} from "../shared/globe";
import {Injectable} from "@angular/core";
import {FirebaseService} from "./firebase.service";
import {UtilService} from "./util.service";
import TWEEN from "@tweenjs/tween.js";
import {TweenModel} from "../shared/TweenModel";

@Injectable()
export class TweenChainService {

  tweenConfig: TweenModel;

  constructor(private util: UtilService) {
  }

  createTween(globe: Globe, place: any) {

    const {camera, control} = globe;

      this.tweenConfig = new TweenModel(place.id, 3.5, 1.3, 4000, 1000);

      let markerPosition = this.util.convertLatLonToVec3(place.latitude, place.longitude, this.tweenConfig.radiusToTarget, this.tweenConfig.radiusToSpace);

      const tweenSpace = new TWEEN.Tween(camera.position);
      tweenSpace
        .to(markerPosition.spaceVectorPosition, this.tweenConfig.speedToSpace)
        .easing(TWEEN.Easing.Quadratic.Out)
        .onStart(() => control.enabled = false)
        .onUpdate(() => globe.camera.updateProjectionMatrix())


      const tweenTarget = new TWEEN.Tween(camera.position);
      tweenTarget
        .to(markerPosition.spaceVectorPosition, this.tweenConfig.speedToSpace)
        .easing(TWEEN.Easing.Quadratic.Out)
        .onUpdate(() => globe.camera.updateProjectionMatrix());

      tweenSpace.chain(tweenTarget.delay(3000)).delay(3000);
      globe.addTween(tweenTarget);

      const tweenBackSpace = new TWEEN.Tween(camera.position);
      tweenTarget
        .to(markerPosition.spaceVectorPosition, this.tweenConfig.speedToSpace)
        .easing(TWEEN.Easing.Quadratic.Out)
        .onStart(() => control.enabled = false)
        .onUpdate(() => globe.camera.updateProjectionMatrix())
        .onComplete(() => control.enabled = true);

        tweenSpace.chain(tweenTarget.delay(3000).chain(tweenBackSpace).delay(300)).delay(3000);
        globe.addTween(tweenTarget);
  }
}

  //
// let targetPosition = this.util.convertLatLonToVec3(place.latitude,place.longitude, radiusToTarget, radiusSpace);
//
// let goToSpaceView = new TWEEN.Tween(camera.position).to(targetPosition.spaceVectorPosition, this.runnerConfig.transitionSpaceSpeed ).easing(TWEEN.Easing.Quadratic.Out)
//   .onStart(() => {
//     console.log("Start 1 " + this);
//     this.enableControl(control)
//   })
//   .onUpdate(() => {
//     console.log("Update 1 " + this);
//     globe.camera.updateProjectionMatrix()
//   })
//   .onComplete(() => {
//     console.log("Complete 1 " + this);
//     this.disableControl(control)
//   });
//
//
//
// let gotToTargetView = new TWEEN.Tween(camera.position).to(targetPosition.targetVectorPosition, this.runnerConfig.transitionToTargetSpeed ).easing(TWEEN.Easing.Quadratic.Out)
//   .onUpdate(() => {
//     console.log("Update 2 " + this);
//     camera.updateProjectionMatrix();
//   })
//   .onStart(() => {
//     console.log("Start 2 " + this);
//     this.enableControl(control);
//   })
//   .onComplete(() => {
//     console.log("Complete 2 " + this);
//     this.disableControl(control);
//   });
//
// let backToSpaceView = new TWEEN.Tween(camera.position).to(targetPosition.spaceVectorPosition, this.runnerConfig.transitionToTargetSpeed ).easing(TWEEN.Easing.Quadratic.Out)
//   .onStart(() => {
//     this.enableControl(control);
//   })
//   .onUpdate(() => {
//     camera.updateProjectionMatrix();
//   })
//   .onComplete(() => {
//     this.disableControl(globe.control);
//     this.runnerConfig.processId += 1;
//
//     if(this.runnerConfig.processId >= places.length){
//       this.runnerConfig.processId = 0;
//     }
//
//     if(this.runnerConfig.runnersContainer.length > 0) {
//       this.runnerConfig.runnersContainer[this.runnerConfig.processId].start();
//     }
//   });
//
// goToSpaceView.chain(gotToTargetView.chain(backToSpaceView.delay(3000)));
//
// this.runnerConfig.runnersContainer.push(goToSpaceView);
