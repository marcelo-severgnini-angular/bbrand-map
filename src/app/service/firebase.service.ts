import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';

export class FirebaseService {
  constructor(private db: AngularFireDatabase) { }

  placeList: any;

  getPlaces() {
    return this.db.database.ref('/marked-places/places');
  }
}
