// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCT5kOaMh_s0w16M9S1cNLlIn6mCrfhWJ8",
    authDomain: "geo-navigation.firebaseapp.com",
    databaseURL: "https://geo-navigation.firebaseio.com",
    projectId: "geo-navigation",
    storageBucket: "geo-navigation.appspot.com",
    messagingSenderId: "856376774252"
  },
  basePath: ""
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
